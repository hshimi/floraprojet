<?php
namespace App\Command;

use App\Service\ArticleAggregator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AggregateFromRssCommand extends Command
{
    protected static $defaultName = 'app:aggregate-from-rss';
    private $artcileAggregator;
    private $logger;

    public function __construct(ArticleAggregator $artcileAggregator, LoggerInterface $logger)
    {
        $this->artcileAggregator = $artcileAggregator;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Aggregates articles from an Rss feed')
            ->addOption(
                'rssUrl',
                null,
                InputOption::VALUE_REQUIRED,
                'RSS URL to fetch articles from'
            )
            ->addOption(
                'sourceName',
                null,
                InputOption::VALUE_REQUIRED,
                'name of the article source'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rssUrl = $input->getOption('rssUrl');
        $sourceName = $input->getOption('sourceName');

        try {
            $this->artcileAggregator->appendRss($sourceName, $rssUrl);
            $output->writeln('Articles successfully aggregated from the RSS feed.');
            $this->logger->info('Articles successfully aggregated from the RSS feed.', ['sourceName' => $sourceName, 'rssUrl' => $rssUrl]);
        } catch (\Exception $e) {
            $output->writeln('Failed to aggregate articles from the RSS feed:' . $e->getMessage());
           $this->logger->error('Failed to aggregate articles from the RSS feed', ['error' => $e->getMessage()]);
        }

        return Command::SUCCESS;
    }
}