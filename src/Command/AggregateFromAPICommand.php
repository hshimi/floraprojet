<?php
namespace App\Command;

use App\Service\ArticleAggregator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AggregateFromAPICommand extends Command
{
    protected static $defaultName = 'app:aggregate-from-api';
    private $articleAggregator;
    private $logger;

    public function __construct(ArticleAggregator $articleAggregator, LoggerInterface $logger)
    {
        $this->articleAggregator = $articleAggregator;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure()
    {
        $this
                ->setDescription('Aggregates articles from an API sourec.')
                ->addOption(
                    'apiUrl',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'API URL to fetch articles from'
                    )
                ->addOption(
                    'sourceName',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'Name of the article sourec'
                );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $apiUrl = $input->getOption('apiUrl');
        $sourceName = $input->getOption('sourceName');

        try {
            $this->articleAggregator->appendFromAPI($apiUrl, $sourceName);
            $output->writeln('Articles successfully aggregated from the API.');
            $this->logger->info('Artilces successfully aggregated from the API.', ['apiUrl' => $apiUrl, 'sourceName' => $sourceName]);  
        } catch (\Exception $e) {
           $output->writeln('Failed to aggregate articles from the API:' . $e->getMessage());
           $this->logger->error('Failed to aggregate articles from the API', ['error' => $e->getMessage()]);
        }

        return Command::SUCCESS;
    }
}