<?php
namespace App\Command;

use App\Service\ArticleAggregator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AggregateFromDatabaseCommand extends Command
{
    protected static $defaultName = 'app:aggregateèfrom-database';
    private $articleAggregator;
    private $logger;

    public function __construct(ArticleAggregator $articleAggregator, LoggerInterface $logger)
    {
        $this->articleAggregator = $articleAggregator;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Aggregates articles from a database source.')
            ->addOption(
                'dbHost',
                null,
                InputOption::VALUE_REQUIRED,
                'Database host'
            )
            ->addOption(
                'dbName',
                null,
                InputOption::VALUE_REQUIRED,
                'Database name'
            )
            ->addOption(
                'dbUser',
                null,
                InputOption::VALUE_REQUIRED,
                'Database user'
            )
            ->addOption(
                'dbPassword',
                null,
                InputOption::VALUE_REQUIRED,
                'Database password'
            )
            ->addOption(
                'sourceName',
                null,
                InputOption::VALUE_REQUIRED,
                'Name of the article source'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dbHost = $input->getOption('dbHost');
        $dbName = $input->getOption('dbName');
        $dbUser = $input->getOption('dbUser');
        $dbPassword = $input->getOption('dbPassword');
        $sourceName = $input->getOption('sourceName');

        try {
            $this->articleAggregator->appendDatabase($dbHost, $dbUser, $dbPassword, $dbName, $sourceName);
            $output->writeln('Articles successfully aggregated from the database.');
            $this->logger->info('Articles successfully aggregated from the database.');
        } catch (\Exception $e) {
            $output->writeln('Failed to aggregate articles from the database:' . $e->getMessage());
           $this->logger->error('Failed to aggregate articles from the database', ['error' => $e->getMessage()]);
        }

        return Command::SUCCESS;
    }
}