<?php
namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use SimpleXMLElement;
use App\Entity\Article;
use App\Entity\Source;

class ArticleAggregator
{
    private $entityManager;
    private $httpClient;

    public function __construct(EntityManagerInterface $entitManager, HttpClientInterface $httpClient)
    {
        $this->entityManager = $entitManager;
        $this->httpClient = $httpClient;
    }

    public function appendRss($sourceName, $feedUrl)
    {
        $rss = simplexml_load_file($feedUrl);
        $source = $this->getSource($sourceName, 'rss');

        foreach ($rss->channel->item as $item) {
            $article = new Article();
            $article->setName($item->name);
            $article->setContent($item->content);
            $article->setSource($source);

            $this->entityManager->persist($article);
        }

        $this->entityManager->flush();
    }

    private function getSource($name, $type): Source
    {
        $source = $this->entityManager->getRepository(Source::class)->findOneBy(['name' => $name]);

        if (!$source) {
            $source = new Source();
            $source->setName($name);
            $source->setType($type);
            $this->entityManager->persist($source);
            $this->entityManager->flush();
        }

        return $source;
    }

    public function appendDatabase($host, $username, $password, $dbName, $sourceName)
    {
        $conn = new \Doctrine\DBAL\Connection([
            'dbname' => $dbName,
            'user' => $username,
            'password' => $password,
            'host' => $host,
            'driver' => 'pdo_mysql',
        ], new \Doctrine\DBAL\Driver\PDOMySql\Driver());

        try {
            $conn->connect();
        } catch (\Doctrine\DBAL\Exception\ConnectionException $e) {
            echo "Erreur de connexion à la base de données externe: " . $e->getMessage();
            return;
        }

        $sql = "SELECT * FROM article"; 
        $stmt = $conn->executeQuery($sql);

        $source = $this->getSource($sourceName, 'database');

        while ($row = $stmt->fetchAssociative()) {
            $article = new Article();
            $article->setName($row['name']);
            $article->setContent($row['content']);
            $article->setSource($source);

            $this->entityManager->persist($article);
        }

        $this->entityManager->flush();
    }

    public function appendFromAPI($apiUrl, $sourceName)
    {
        try {
            $response = $this->httpClient->request('GET', $apiUrl);
            
            if ($response->getStatusCode() === 200) {
                $content = $response->getContent();
                $articlesData = json_decode($content, true);
                
                $source = $this->getSource($sourceName, 'api');

                foreach ($articlesData as $articleData) {
                    $article = new Article();
                    $article->setName($articleData['name']);
                    $article->setContent($articleData['content']);
                    $article->setSource($source);

                    $this->entityManager->persist($article);
                }

                $this->entityManager->flush();
            }
        } catch (\Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface $e) {
            echo "Erreur lors de la récupération des articles depuis l'API: " . $e->getMessage();
        }
    }
}