<?php
namespace App\EntityListener;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Symfony\Bundle\SecurityBundle\Security;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::prePersist, entity: Article::class )]
class ArticleEntityListener
{
    public function __construct(
        private Security $security
    )
    {
        
    }

    public function prePersist(Article $article)
    {
        $article->setAuthor($this->security->getUser());
    }
}