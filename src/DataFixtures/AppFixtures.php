<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Article;
use App\Entity\Source;
use App\Entity\User;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;   
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        
        for ($j = 0; $j < 5; $j++) { // Par exemple, créons 5 sources différentes
            $source = new Source();
            $source->setName($faker->word(3, true)); // Utilisons le nom d'une compagnie comme nom de source
            $source->setType($faker->randomElement(['rss', 'api', 'database'])); // Type aléatoire

            $manager->persist($source);

            for ($k = 0 ; $k < 5; $k++) {
                $user = new User();
                $password = $this->hasher->hashPassword($user, 'password');

                $user->setEmail($faker->email)
                    ->setPassword($password)
                    ->setRoles(['ROLE_USER']);
                    $manager->persist($user);
            }
           
            for ($i = 1; $i <= 10; $i++) {
                $article = new Article();
                $article->setName($faker->word(6, true));
                $article->setContent($faker->word(3, true));
                $article->setSource($source);
                $article->setAuthor($user);
                $manager->persist($article);
            }
        
        }
        $manager->flush();
    }
}
